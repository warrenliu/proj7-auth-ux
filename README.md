# Project 7: Adding authentication and user interface to brevet time calculator service

## Authors
* Init by M Young
* Revised by Yiran Liu
* yiranl@uoregon.edu

## Ports
* Port 5000: ACP Calculator Api
* Port 5001: Login app

## Use
```
cd DockerRestAPI
```
* have to cd DcolerRestAPI folder first
* Then compose up
```
docker-compose up --build
```
